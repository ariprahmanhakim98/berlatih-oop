<?php 
require 'animal.php';
require 'Ape.php';
require 'Frog.php';


$sheep = new Animal("shaun");

echo $sheep->getName(); // "shaun"
echo "<br>";
echo $sheep->getLegs(); // 2
echo "<br>";
echo $sheep->get_cold_blooded(); // false
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->getName();
echo "<br>";
echo $sungokong->getLegs();
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<br>";
echo "<br>";
$kodok = new Frog("buduk");
echo $kodok->getName();
echo "<br>";
echo $kodok->getLegs();
echo "<br>";
$kodok->jump() ; // "hop hop"
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>