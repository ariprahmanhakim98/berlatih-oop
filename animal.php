<?php 

class Animal{
	public $name;
	public $legs = 2;
	public $cold_blooded = "false";

	function __construct($nama){
		$this->name = $nama;
	}

	function getName(){
		return $this->name;
	}

	function getLegs(){
		return $this->legs;
	}

	function get_cold_blooded(){
		return $this->cold_blooded;
	}
}

 ?>