<?php 

class Frog extends Animal {
	
	public $name;
	public $legs = 4;

	function __construct($nama){
		$this->name = $nama;
	}

	function getName(){
		return $this->name;
	}

	
	function getLegs(){
		return $this->legs;
	}

	public function jump(){
		echo "hop hop";
	}
}


?>